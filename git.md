
Git
===

For aliases and config, see my [.gitconfig](.gitconfig) (self documented)

To update to new git version

	sudo add-apt-repository ppa:git-core/ppa
	sudo apt update
	sudo install git


Use branches :
 - better separation on work in progress
 - can use `push --force` and even a deep git functionnality: squash

Use `git --amend`



Miscellaneous
=============

Git workflows
-------------

### Git flow
develop/master

mais redondance voir mirroir entre les deux branches

### Github flow
que master
6 points fondamentaux :
 - Tout ce qui est sur master est stable et déployable
 - Pour travailler sur quelque-chose, la branche doit avoir un nom significatif. Comme : feature/add-menu…
 - Il faut commiter sur cette branche localement et régulièrement pusher sur une branche du même nom sur le serveur
 - Une fois le développement terminé, ouvrir une pull request sur master pour recueillir du feedback et des tests.
 - Une fois les feux au vert, merger sur master.
 - Déployer directement après le merge


### Gitlab flow
Ajoute un branch production (et même release ou encore pre-prod)



Some tips for Gitlab
====================

CI Variables
------------
 - [Predefined Variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
 - one can also add CI variables to a group: `Settings` > `CI/CD`, `Variables` section



 - to be able to push/pull even outside of the Probayes VPN, use url like:
	ssh://git@code.probayes.net:28129/


