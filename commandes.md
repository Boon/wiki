
PDF
---

[pdftk](https://doc.ubuntu-fr.org/pdftk)

* img > pdf: `convert *.jpg output.pdf`
* concat: `pdftk *.pdf cat output tous.pdf`
* deconcat: `pdftk input.pdf burst`
* deconcat avec prefix: `pdftk input.pdf burst output page_no%d.pdf`
* pivot:
  * 1er page à l'Est mais pas les autres pages :
    ```
    pdftk input.pdf cat 1east 2-end output sortie.pdf
    ```
  * toutes les pages au Sud :
    ```
    pdftk input.pdf cat 1-endsouth output sortie.pdf
    ```
* extraction
    ```
    pdftk input.pdf cat 6 2-5 7-9 output sortie.pdf
    ```
* Compression (pour impression) :
    ```
    gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer -dNOPAUSE -dQUIET -dBATCH  -sPAPERSIZE=a4 -dFIXEDMEDIA -dPDFFitPage  -sOutputFile=output.pdf input.pdf
    ```

Images
------
 - Une image
```
convert -resize 50% image.jpg image.jpg`
```

 - En batch
```
mkdir tmp
for i in *.JPG ; do convert -resize 1500 "$i" "tmp/$i" ; done
```


Pandoc
------

```
pandoc input.md -o output.pdf
```

 - Add links and color them:
   ```
   --reference-links  -V colorlinks=true
   ```
 - don't forget space before list
