

Installation de Ubuntu
======================

Installation
------------
If grub freeze in "APIC", "region" or "DMAR" stuff: in grub menu, cursor on `ubuntu` and type `e`.
Add `nouveau.modeset=0` at the end of `linux...` line and type F10 to launch.

The `Updates and other software` part, after selecting third parties, may take a while, juste wait ;)


Remove unused stuffs
--------------------

Remove arabic and asian fonts

	sudo apt purge fonts-kacst-one fonts-arphic-ukai fonts-arphic-uming 
	sudo apt purge language-pack-zh-hans language-pack-zh-hans-base language-pack-zh-hant language-pack-zh-hant-base fonts-lao fonts-lklug-sinhala fonts-sil-abyssinica fonts-sil-padauk fonts-tibetan-machine fonts-thai-tlwg fonts-khmeros-core fonts-lohit-guru fonts-guru fonts-nanum fonts-noto-cjk fonts-takao-pgothic fonts-guru-extra fonts-tlwg-* fonts-beng* fonts-beng-extra* fonts-deva* fonts-deva-extra* fonts-gargi* fonts-gubbi* fonts-gujr* fonts-gujr-extra* fonts-kalapi* fonts-knda* fonts-telu fonts-pagul
	sudo apt autoremove


Remove unused packages
	sudo apt purge brltty speech-dispatcher mlocate
	sudo apt autoremove


Install proprietary stuffs
--------------------------
Installer les principaux codecs multimédia propriétaires (mp3, flash, wmv, DVD, ...)

	sudo apt install ubuntu-restricted-extras

Installer driver nvidia recommandé ici `ubuntu-drivers devices`

	sudo apt install nvidia-driver-...
	sudo prime-select intel


Move home
---------
https://doc.ubuntu-fr.org/tutoriel/deplacer_home

Mount old drive and sync it in new home:

	sudo mkdir /mnt/data
	rsync -aAX /mnt/data/alban/ /media/alban/0426fc25-c176-485c-8300-7de2f3bff3f7/alban/

Switch in root mode and make `/` writable:
	sudo telinit 1
	mount -o rw,remount /


Find the new filesystem
	fdisk -l
	blkid /dev/sdb3 >> /etc/fstab
	nano /etc/fstab

Modify line like `UUID=0426fc25-c176-485c-8300-7de2f3bff3f7      /home     ext4        defaults        0       2`

Move old home et reboot:
	mv /home /home.bak
	mkdir /home
	reboot

Once eveyrthing is ok, remove old home: `rm -rf /home.bak`.


Note: Change personnal dirs in `~/.config/user-dirs.dirs`


Install stuffs
--------------

	sudo apt install meld git nextcloud-desktop clementine vlc python3.9-venv curlftpfs pdftk-java xournal imagemagick-6.q16 gimp docker-compose
	sudo snap install pycharm-community --classic
	sudo snap install zoom-client
	newgrp docker
	sudo usermod -aG docker $USER

Install DVD reader
------------------
	sudo apt-get install ubuntu-restricted-extras
	sudo dpkg-reconfigure libdvd-pkg
	sudo reboot


Optimisation
------------

[Hide grub menu](https://askubuntu.com/questions/879881/how-can-i-get-my-grub-menu-to-be-hidden-and-have-the-shift-or-esc-keys-show-the)
In `sudo nano /etc/grub.d/30_os-prober`, set `quick_boot="0"`
In `sudo nano /etc/default/grub`, set `GRUB_TIMEOUT=0`.
Then `sudo update-grub`.


https://askubuntu.com/questions/1166486/how-to-decrease-the-boot-time/1166492#1166492
	sudo systemctl disable NetworkManager-wait-online.service


https://askubuntu.com/questions/1053150/nouveau-modeset0


Ubuntu audit
============

Boot stats:

	systemd-analyze
	systemd-analyze critical-chain
	systemd-analyze blame

To see boot errors
	sudo dmesg -lerr
