
A DevOps Wiki
=============

- [Byobu](byobu.md): a command line tools to use on server (detach term, multi screen...)
- [General commandes](commandes.md): like pdf, images
- [Docker](docker.md)
- [Git](git.md)

