
Byobu
=====

Shortcuts
---------
### Create
- `F2`: Create a new tab
- `Shift-F2`: Split horizontally ---
- `Ctrl-F2`: Split vertically   |


### Move
- `Alt-←→`	Change tab
- `Alt-↓↑`	Change session
- `Shift-←→↓↑`	Change split
- `Alt-↟↡`	Move in history


### Rename
- `F8`: Rename tab
- `Ctrl-F8`: Rename session

### Delete
- `Ctrl-F6`: Delete split in focus (delete tab if no split)


### Other
- `F6`: Detach from session
- `F1`: Help
