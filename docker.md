

- build: `docker build -t project/name .`
- Run interactif: `docker run -it project/name bash`
- Connection to an running container: `docker exec -it project/name bash`
- remove untagged images: `docker rmi $(docker images -f "dangling=true" -q)`


Inspection de docker
--------------------

Use dive: `dive project/name (apt install dive)`




Going deeper
============

docker swarm
------------
    docker swarm init
 - see nodes: `docker node ls`
 - see token: `docker swarm join-token worker`

 - Start a stack: `docker stack deploy --compose-file docker-compose.yml easymaint`
 - See stack: `docker stack services easymaint`


 - Stop the stack: `docker stack rm easymaint`



docker context
--------------

    docker context create gulplug --docker "host=ssh://root@51.77.195.234"
    docker context ls
    docker context use gulplug
    docker ps
